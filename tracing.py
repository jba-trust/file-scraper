import logging

TRACE_LOG_LEVEL = logging.DEBUG - 5
TRACE_LOG_NAME = "TRACE"


class TracingLogger(logging.getLoggerClass()):

    def trace(self, msg, *args, **kwargs):
        self.log(TRACE_LOG_LEVEL, msg, *args, **kwargs)


def add_tracing():
    if logging.getLevelName(TRACE_LOG_LEVEL) == TRACE_LOG_NAME:
        return
    logging.addLevelName(TRACE_LOG_LEVEL, TRACE_LOG_NAME)
    logging.setLoggerClass(TracingLogger)
    logging.TRACE = TRACE_LOG_LEVEL
