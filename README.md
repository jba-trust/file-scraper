# File Scraper

File Scraper will look through all files in the given paths for any specified keywords.
It can currently read through both PDFs and plain text files.
Word documents (`.docx`) are planned for later versions.

## Installation

In order to run this file scraper, you must already have [Python 3](https://www.python.org/downloads/) installed.

You can then either clone this repository, or download the [latest release](https://gitlab.com/jba-trust/file-scraper/-/archive/master/file-scraper-master.zip).

Once you have downloaded the source, run the `install.bat` file to create a local environment and install the necessary 
python libraries.

## Usage

Using File Scraper on the command line, having navigated to the `pdf_scraper` folder:

```
env\Scripts\python scrape.py ^
 C:\path\to\search\* ^
 C:\path\to\search\* ^
 --keyword waterbourne ^
 --keyword groundwater ^
 --output findings.txt ^
 --sort file
```

**Arguments**:

  * `--keyword waterbourne` will add *'waterbourne'* as a word to search for. You can include multiple keywords as shown
    in the above example.
  * `--output findings.txt` will output all findings to the file `findings.txt`.
  * `--sort file` will group the findings by files searched.
  * `--sort keyword` will group the findings by keyword.

**Output**:

If an output file is not provided, it will be written to the console.
The output is includes the location within the text, and the surrounding text for any context of the keyword.

## Examples

In the `examples` folder there is an example for windows that searches 
