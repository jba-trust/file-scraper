@set wd=%cd%
@cd %~dp0


@..\env\Scripts\python.exe -u ..\scrape.py ^
 "C:\Users\huwtaylor\Development\pdf_scraper\documents\PhD thesis\*" ^
 "C:\Users\huwtaylor\Development\pdf_scraper\documents\Section 19 Reports\*" ^
 "C:\Users\huwtaylor\Development\pdf_scraper\documents\SFRAs\*" ^
 "C:\Users\huwtaylor\Development\pdf_scraper\documents\spreadsheet_files\*" ^
 "C:\Users\huwtaylor\Development\pdf_scraper\documents\Extras\*" ^
 --keyword groundwater ^
 --output "C:\Users\huwtaylor\Development\pdf_scraper\examples\findings.txt" ^
 --debug


@pause
@cd %wd%
