#!/usr/bin/env python
# coding: utf8

import argparse
import logging
import tracing
import file_scraper
from typing import Optional
from pathlib import Path
from output_grouping import OutputGrouping

tracing.add_tracing()


def main(report_paths: list, keywords: list, sort_by: OutputGrouping, output_file: Optional[Path], 
         quiet: bool, debug: bool, trace: bool):
    logging_level = logging.INFO
    if quiet:
        logging_level = logging.ERROR
    if debug:
        logging_level = logging.DEBUG
    if trace:
        logging_level = logging.TRACE
    file_scraper.scrape_paths(list(report_paths), keywords, output_file, sort_by, logging_level)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Finds keywords and their context in files.", prog="File Scraper")
    parser.add_argument("--version",
                        action="version", version="%(prog)s v0.7.0")
    parser.add_argument("--keyword",
                        action="append", dest="keywords",
                        type=str,
                        help="The keyword to search for")
    parser.add_argument("--sort",
                        nargs="?", dest="sort_by",
                        type=OutputGrouping, choices=OutputGrouping.values(), default=OutputGrouping.FILENAME,
                        help="How to group the results")
    # noinspection PyTypeChecker
    parser.add_argument("--output", "-o",
                        nargs="?", dest="output_file",
                        type=Path, default=None,
                        help="The file to write the results to")
    parser.add_argument("--quiet", "-q",
                        action="store_true", dest="quiet",
                        default=False,
                        help="Stifle nonessential output")
    parser.add_argument("--debug",
                        action="store_true", dest="debug",
                        default=False,
                        help="Print debugging information")
    parser.add_argument("--trace",
                        action="store_true", dest="trace",
                        default=False,
                        help="Print detailed execution information")
    # noinspection PyTypeChecker
    parser.add_argument("report_paths",
                        nargs="+",
                        type=Path,
                        help="The paths to the reports to search")
    args = parser.parse_args()
    main(**vars(args))
