from enum import Enum


class OutputGrouping(Enum):
    FILENAME = "file"
    KEYWORD = "keyword"

    @classmethod
    def values(cls) -> set:
        return set([e.value for e in OutputGrouping])
