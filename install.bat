@set wd=%cd%
@cd %~dp0

@if exist C:\Windows\py.exe (
    @py -3 -m pip install --upgrade pip
    @py -3 -m venv env
    @env\Scripts\pip.exe install -r REQUIREMENTS.txt
) else (
    @echo This tool requires python 3.5 or higher to be installed.
    @echo Could not find C:\Windows\py.exe
)

@pause
@cd %wd%
