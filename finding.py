class Finding:

    def __init__(self, filename: str, page: int, keyword: str, surrounding_text: str, span: tuple):
        self.filename = filename
        self.page = page
        self.keyword = keyword
        self.surrounding_text = surrounding_text
        self.span = span
