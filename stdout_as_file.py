import sys
import contextlib
from typing import Optional
from pathlib import Path


@contextlib.contextmanager
def open_file_or_stdout(filename: Optional[Path] = None, mode='w'):
    if filename is not None:
        # noinspection PyTypeChecker
        fh = filename.open(mode, encoding="utf8")
    else:
        fh = sys.stdout

    try:
        yield fh
    finally:
        if fh is not sys.stdout:
            fh.close()