import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="file-scraper-IMP1",
    version="0.1.0",
    author="Huw Taylor",
    author_email="Huw.Taylor@jbaconsulting.com",
    description="A file-scraping tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jba-trust/file-scraper",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
