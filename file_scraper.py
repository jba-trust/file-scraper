import sys
import tracing
import re
import glob
import PyPDF2
import logging
from openstruct import OpenStruct
from datetime import datetime
from typing import Optional
from pathlib import Path
from finding import Finding
from output_grouping import OutputGrouping
from stdout_as_file import open_file_or_stdout


tracing.add_tracing()

logging.basicConfig(format="[%(name)s] %(levelname)s: %(message)s")
logger = logging.getLogger("File Scraper")

config = OpenStruct()
config.context_width = None
config.output_filepath = None
config.output_grouping = OutputGrouping.FILENAME
config.verbose = True
config.debug = False
config.trace = False


def scrape_text(text: str, keywords: list, keyword_regexps: dict, origin: str, page_number: int) -> list:
    # TODO:
    #  Have this method include all findings
    #  It currently only finds the first instance of a keyword in a paragraph, and ignores any subsequent ones.
    #  This could be a while loop, with an increasing `start_point` to `search` from? Or using `findall`, or whatever.
    findings = []
    paragraphs = text.split("\n\n")
    for paragraph in paragraphs:
        for keyword in keywords:
            finding = keyword_regexps[keyword].search(paragraph)
            if finding:
                match = finding.group(0)
                span = finding.span()
                findings.append(Finding(origin, page_number, match, paragraph, span))
    return findings


# noinspection PyTypeChecker
def extract_text(filepath: Path):
    # PDFs don't have an internal representation of a paragraph necessarily, and word documents don't have an internal
    # representation of a page. Pages are much more human-friendly ('there was a match on page x' is easier to find than
    # 'there was a match in paragraph Y'), so when known, they are used.
    if filepath.suffix.lower() == ".pdf":
        with open(filepath, 'rb') as pdf_file:
            pdf_document = PyPDF2.PdfFileReader(pdf_file)
            number_padding = len(str(pdf_document.numPages))
            for page_number in range(pdf_document.numPages):
                n = str(page_number + 1).zfill(number_padding)
                logger.trace(f"  {n} / {pdf_document.numPages} pages")
                page = pdf_document.getPage(page_number)
                yield page.extractText(), page_number
    if filepath.suffix.lower() == ".txt":
        with open(filepath, 'r') as txt_file:
            yield txt_file.read(), 1
    if filepath.suffix.lower() == ".docx":
        logger.warning("Word documents are not yet supported.")
        yield "", 1


def scrape_file(filepath: Path, keywords: list):
    keyword_regexps = {}
    for keyword in keywords:
        keyword_regexps[keyword] = re.compile(keyword, re.IGNORECASE)
    findings = []
    logger.debug(f"Searching {filepath}")
    for text, page in extract_text(filepath):
        findings.extend(scrape_text(text, keywords, keyword_regexps, str(filepath), page))
    logger.debug(f"Finished searching {filepath}\n")
    return findings


def bold_output(text, output):
    # See ANSI escape codes http://ascii-table.com/ansi-escape-sequences-vt-100.php
    bold_on = "\33[1m"
    bold_off = "\33[0m"
    if output == sys.stdout:
        return bold_on + text + bold_off
    else:
        return text


def group_findings(findings: list) -> dict:
    grouped_findings = {}
    if config.output_grouping == OutputGrouping.FILENAME:
        field = lambda f: f.filename
    else:
        field = lambda f: f.keyword
    for finding in findings:
        if field(finding) not in grouped_findings:
            grouped_findings[field(finding)] = []
        grouped_findings[field(finding)].append(finding)
    return grouped_findings


def output_findings(grouped_findings: dict, keywords: list):
    total_findings = 0
    with open_file_or_stdout(config.output_filepath) as out:
        timestamp = datetime.utcnow()
        out.write(f"Searched on {timestamp}\n")
        if not grouped_findings:
            out.write("No results found.\n")
            return
        for grouping in grouped_findings:
            findings = grouped_findings[grouping]
            out.write(bold_output(f"# {grouping}\n", out))
            if config.output_grouping == OutputGrouping.FILENAME:
                for keyword in keywords:
                    keyword_findings = [f for f in findings if f.keyword == keyword]
                    total_findings += len(keyword_findings)
                    out.write(f"{len(keyword_findings)} instances found of '{keyword}':\n")
                    for finding in keyword_findings:
                        start = 0
                        if config.context_width is not None:
                            start = max(0, finding.span[0] - config.context_width)
                        end = len(finding.surrounding_text)
                        if config.context_width is not None:
                            end = min(len(finding.surrounding_text), finding.span[1] + config.context_width)
                        context = finding.surrounding_text[start:end]
                        context = context.replace(finding.keyword, bold_output(finding.keyword, out))
                        context = context.replace("\n", "\n    ")
                        page = finding.page
                        out.write(f"  * [Page {page}]\n    ...{context}...\n")
            else:
                out.write("\n")
                for finding in findings:
                    out.write(f"{finding.keyword}\n")
                    out.write(f"On page {finding.page} in {finding.filename}\n")
                    out.write("\n")
            out.write("\n")
    if out != sys.stdout:
        if grouped_findings:
            logger.info(f"Findings written to {config.output_filepath} ({total_findings} total findings).")
        else:
            logger.info("No results found.")


def output_errors(errors):
    with open_file_or_stdout(config.output_filepath, mode='a') as out:
        out.write(f"{len(errors)} error(s) found")
        for filename, e in errors:
            out.write(f"Error processing {filename}:\n")
            out.write(str(e))
            out.write("\n")
    if out != sys.stdout:
        logger.error(f"{len(errors)} error(s) found")
        logger.error(f"Errors written to {config.output_filepath}.")


def scrape_paths(filepaths: list, keywords: list, output_filepath: Optional[Path], output_grouping: OutputGrouping,
                 logging_level=logging.INFO):
    logger.setLevel(logging_level)
    config.output_filepath = output_filepath
    config.output_grouping = output_grouping

    logger.debug(f"Searching for keywords: {keywords}.")
    file_count = 0
    findings = []
    errors = []
    for path in filepaths:
        for filename in glob.glob(str(path)):
            file_count += 1
            try:
                results = scrape_file(Path(filename), keywords)
                findings.extend(results)
            except Exception as e:
                errors.append([filename, e])
    logger.info(f"Searched {file_count} files.")
    grouped_findings = group_findings(findings)
    output_findings(grouped_findings, keywords)
    if errors:
        output_errors(errors)
